1 Declarações e uso de variáveis, tipos fundamentais (int, float, char, double). Operadores
aritméticos, de atribuição e de endereços. entrada padrão de dados. Exercícios
2 – Operadores relacionais e operadores lógicos.
3 – Estruturas de repetição: for, foreach, while e do-while;
4 - Estruturas condicionais: if, if-else, switch-case.
5 – Conceito de função e modularização;
6 – Implementação de funções sem retorno, com e sem parâmetros
7 – Implementação de funções com retorno, com e sem parâmetros
8 - Organização de funções em arquivos de cabeçalho
9 - Vetores: declaração, referenciação e verificação de limites.
10 - Strings: funções específicas para manipulação de cadeias de caracteres
11 - Matrizes: declaração, referenciação e verificação de limites.
12 - Noções de ponteiros
13 - Uso de ponteiros em funções
14 – Spring boot
     Criando uma Api com Spring Boot
     Usando Postam para Consulta essa API
     Usando banco de dados na api
     Verificar a Segurança das Apis
     Verificar vulnerabilidades
     Proteger de vulnerabilidades
15 – Projeto de um API